# Sampagora - Site

## Instalação de dependências
    php composer.phar install

## Configuração de web server

### PHP CLI Server
    APPLICATION_ENV=development php -d variables_order=EGPCS -S localhost:8888 -t public public/index.php

### Apache
    <VirtualHost *:80>
        ServerName sampa-admin.localhost
        DocumentRoot /var/www/html/agora-site/public
        SetEnv APPLICATION_ENV "development"

        <Directory /var/www/html/agora-site/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
