<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json','Content-Type' => 'application/json' ]]); //GuzzleHttp\Client
        $response = $client->get('http://api.agora.local/agora/event');
        #$response = $client->get('http://api.sampagora.com.br/agora/event');
        $contents = json_decode($response->getBody()->getContents());

        return view('index', ['events' => $contents->_embedded->content]);
    }
}
