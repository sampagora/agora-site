@extends('templates.main')

@section('main')
    <div class="container">
        <div class="card-columns">
            @foreach ($events as $event)
                <div class="card">
                    <img alt="Card image cap" class="card-img-top img-fluid" src="https://www.ticket360.com.br/public/files/eventos/banners/8186.jpg">
                    <div class="card-block">
                        <h4 class="card-title">{{ $event->name }}</h4>
                        @if($event->categories)
                           {{ $event->categories }}
                        @endif
                        @if($event->place)
                            <h6 class="card-title">{{ $event->place->name }}</h4>
                        @endif
                        <i class="fa fa-map-marker" aria-hidden="true"> {{ $event->address }}</i>
                        <ul class="list-group list-group-flush">
                            @foreach ($event->performers as $eventPerformer)
                                <li class="list-group-item">{{ $eventPerformer->performer->name }}</li>
                            @endforeach
                        </ul>
                        @if($event->homepages)
                            @foreach ($event->homepages as $type => $url)
                                <a href="https://{{ $url }}" target="_blank" class="fa fa-{{ $type }}"></a>
                            @endforeach
                        @endif
                    </div>
                </div>
            @endforeach
        </div>	
    </div>
@endsection